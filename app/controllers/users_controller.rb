class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :require_user, only: [:edit, :update]
  before_action :require_same_user, only: [:edit, :update, :destroy]

  def show 
    @user_articles = @user.articles.order("created_at DESC").paginate(page: params[:page], per_page: 5)
  end

  def index
    @users = User.order("created_at ASC").paginate(page: params[:page], per_page: 20)
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(users_params)
    if @user.save
      session[:user_id] = @user.id
      flash[:notice] = "Hi #{ @user.username }, welcome to the ALPHA-BLOG, you have been usccesfully signed up"
      redirect_to articles_path
    else
      render 'new'
    end
  end

  def update
    if @user.update(users_params)
      flash[:notice] = "Your account is updated succesfully"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    @user.destroy
    session[:user_id] = nil if @user == current_user
    flash[:notice] = "Account and all associated articles successfully deleted"
    redirect_to root_path
  end

  private

  def users_params
    params.require(:user).permit(:username, :email, :password)
  end

  def set_user
    @user = User.find(params[:id])
  end

  def require_same_user
    if current_user != @user and !current_user.admin?
      flash[:alert] = "#{current_user.name} can not edit #{@user.name}"
      redirect_to @user
    end
  end
  
end