module ArticleHelper
  def LastUpdated(article)
    if article.updated_at != article.created_at
      # "<small>Created <%= time_ago_in_words(article.updated_at) %> ago, edited <%= time_ago_in_words(article.created_at) %> ago</small>"
      "Created #{time_ago_in_words(article.created_at)} ago, edited #{time_ago_in_words(article.updated_at)}"
    else
      # "<small>Created <%= time_ago_in_words(article.updated_at) %> ago</small>"
      "Created #{time_ago_in_words(article.created_at)} ago"
    end
  end
end