require "test_helper"

class ListCategories < ActionDispatch::IntegrationTest

  def setup
    @category = Category.create(name: "sports")
    @category2 = Category.create(name: "programming")
    @user = User.create(username: "john", email: "john@example.com", password: "password", admin: true)
  end

  test "should show catgeories listing as admin" do
    sign_in_as(@user, "password")
    get categories_path
    assert_template 'categories/index'
    assert_select "a[href=?]", category_path(@category), text: @category.name
    assert_select "a[href=?]", category_path(@category2), text: @category2.name
  end
end